package com.otdengenharia.appotdoac.api

import com.otdengenharia.appotdoac.model.ApiOac
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response

interface ApiHelper {
    suspend fun getDispositivesOac() : List<ApiOac>

    suspend fun sendDispositiveOac(requestBody: RequestBody) : Response<ResponseBody>

    suspend fun uploadImage(name : String,image : String) : Response<ResponseBody>

}