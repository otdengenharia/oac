package com.otdengenharia.appotdoac.api

import com.otdengenharia.appotdoac.model.ApiOac
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    @GET("oac/read.php")
    suspend fun getDispositivesOac() : List<ApiOac>

    @Headers( "Content-Type: application/json" )
    @POST("oac/write.php")
    suspend fun sendDispositiveOac(@Body requestBody: RequestBody) : Response<ResponseBody>

    @FormUrlEncoded
    @POST("oac/uploadImage.php")
    suspend fun uploadImage(@Field("name") name : String, @Field("image") image : String) : Response<ResponseBody>

}