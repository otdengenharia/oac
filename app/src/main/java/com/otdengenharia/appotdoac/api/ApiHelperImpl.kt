package com.otdengenharia.appotdoac.api

import com.otdengenharia.appotdoac.model.ApiOac
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response

class ApiHelperImpl(private val apiService: ApiService) : ApiHelper {
    override suspend fun getDispositivesOac(): List<ApiOac> {
        return apiService.getDispositivesOac()
    }

    override suspend fun sendDispositiveOac(requestBody: RequestBody): Response<ResponseBody> {
        return apiService.sendDispositiveOac(requestBody)
    }

    override suspend fun uploadImage(name: String, image: String): Response<ResponseBody> {
        return apiService.uploadImage(name,  image)
    }
}

