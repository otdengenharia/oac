package com.otdengenharia.appotdoac.model

data class ApiFotoOac (
    var id: Long = 0L,
    var ficha: Long,
    var nome: String,
    var path: String?,
    var observacao: String,
    var data: String,
    var sincronizado: String? = "N"
)