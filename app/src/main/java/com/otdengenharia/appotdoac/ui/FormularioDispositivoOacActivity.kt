package com.otdengenharia.appotdoac.ui

import android.Manifest
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.otdengenharia.appotdoac.R
import com.otdengenharia.appotdoac.api.ApiHelperImpl
import com.otdengenharia.appotdoac.api.RetrofitBuilder
import com.otdengenharia.appotdoac.databinding.ActivityFormularioDispositivoOacBinding
import com.otdengenharia.appotdoac.local.FormularioDispositivoOacViewModel
import com.otdengenharia.appotdoac.local.db.DatabaseBuilder
import com.otdengenharia.appotdoac.local.db.DatabaseHelperImpl
import com.otdengenharia.appotdoac.local.entity.Oac
import com.otdengenharia.appotdoac.utils.*

class FormularioDispositivoOacActivity : AppCompatActivity() {

    private lateinit var binding: ActivityFormularioDispositivoOacBinding
    private lateinit var viewModel: FormularioDispositivoOacViewModel
    private var dispositivo: Oac? = null
    private var codAuto: Long = 0L
    private var fichaReferencia: String = ""
    private var foto1: String? = null
    private var foto2: String? = null
    private lateinit var gpsTracker: GpsTracker
    private lateinit var sharedPreferences: SharedPreferences

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFormularioDispositivoOacBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setupToolbar()
        solicitaPermissaoLocation()
        checkboxDinamicoDiagnostico()
        setupViewModel()
        configuraSpinner()

        binding.activityFormularioDispositivoOacBtnCoordenada.setOnClickListener {
            getLocation("coordenada")
        }

        binding.activityFormularioDispositivoOacBtnCamera.setOnClickListener {
            val intent = Intent(
                    this@FormularioDispositivoOacActivity,
                    FotoOacActivity::class.java
            ).apply {
                putExtra("dispositiveOac", dispositivo)
            }
            startActivity(intent)
        }
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "OAC"
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(
                ApiHelperImpl(RetrofitBuilder.apiService),
                DatabaseHelperImpl(DatabaseBuilder.getInstance(applicationContext))
            )
        )[FormularioDispositivoOacViewModel::class.java]
    }

    private fun getEstadoDeConservacao() {
        if (binding.activityFormularioDispositivoOacCheckboxOk.isChecked) {
            binding.activityFormularioDispositivoOacTextinputEstadoDeConservacao.text = "BOM"
            binding.activityFormularioDispositivoOacTextinputEstadoDeConservacao.setTextColor(
                resources.getColor(
                R.color.Green
            ))
        } else if (binding.activityFormularioDispositivoOacCheckboxAssoreado.isChecked ||
                binding.activityFormularioDispositivoOacCheckboxAfogado.isChecked ||
                binding.activityFormularioDispositivoOacCheckboxErosao.isChecked ||
                binding.activityFormularioDispositivoOacCheckboxFissurasOuTrincasDanificada.isChecked) {
            binding.activityFormularioDispositivoOacTextinputEstadoDeConservacao.text = "PRECARIO"
            binding.activityFormularioDispositivoOacTextinputEstadoDeConservacao.setTextColor(
                resources.getColor(
                R.color.Red
            ))
        } else {
            binding.activityFormularioDispositivoOacTextinputEstadoDeConservacao.text = "REGULAR"
            binding.activityFormularioDispositivoOacTextinputEstadoDeConservacao.setTextColor(
                resources.getColor(
                R.color.Orange
            ))
        }
    }

    private fun checkboxDinamicoDiagnostico() {
        binding.activityFormularioDispositivoOacCheckboxOk.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.activityFormularioDispositivoOacCheckboxLimpeza.isChecked = false
                binding.activityFormularioDispositivoOacCheckboxAssoreado.isChecked = false
                binding.activityFormularioDispositivoOacCheckboxAfogado.isChecked = false
                binding.activityFormularioDispositivoOacCheckboxErosao.isChecked = false
                binding.activityFormularioDispositivoOacCheckboxTestaOuAlaDanificada.isChecked = false
                binding.activityFormularioDispositivoOacCheckboxTubulacaoDanificada.isChecked = false
                binding.activityFormularioDispositivoOacCheckboxCaixaDanificada.isChecked = false
                binding.activityFormularioDispositivoOacCheckboxFissurasOuTrincas.isChecked = false
                binding.activityFormularioDispositivoOacCheckboxFissurasOuTrincasDanificada.isChecked = false
                binding.activityFormularioDispositivoOacCheckboxTampaDanificadaOuInexistente.isChecked = false

            }
        }

        binding.activityFormularioDispositivoOacCheckboxLimpeza.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.activityFormularioDispositivoOacCheckboxOk.isChecked = false
            }
        }

        binding.activityFormularioDispositivoOacCheckboxAssoreado.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.activityFormularioDispositivoOacCheckboxOk.isChecked = false
            }
        }

        binding.activityFormularioDispositivoOacCheckboxAfogado.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.activityFormularioDispositivoOacCheckboxOk.isChecked = false
            }
        }

        binding.activityFormularioDispositivoOacCheckboxErosao.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.activityFormularioDispositivoOacCheckboxOk.isChecked = false
            }
        }

        binding.activityFormularioDispositivoOacCheckboxTestaOuAlaDanificada.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.activityFormularioDispositivoOacCheckboxOk.isChecked = false
            }
        }

        binding.activityFormularioDispositivoOacCheckboxTubulacaoDanificada.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.activityFormularioDispositivoOacCheckboxOk.isChecked = false
            }
        }

        binding.activityFormularioDispositivoOacCheckboxCaixaDanificada.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.activityFormularioDispositivoOacCheckboxOk.isChecked = false
            }
        }

        binding.activityFormularioDispositivoOacCheckboxFissurasOuTrincas.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.activityFormularioDispositivoOacCheckboxOk.isChecked = false
                binding.activityFormularioDispositivoOacCheckboxFissurasOuTrincasDanificada.isChecked = false
            }
        }

        binding.activityFormularioDispositivoOacCheckboxFissurasOuTrincasDanificada.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.activityFormularioDispositivoOacCheckboxOk.isChecked = false
                binding.activityFormularioDispositivoOacCheckboxFissurasOuTrincas.isChecked = false
            }
        }


        binding.activityFormularioDispositivoOacCheckboxTampaDanificadaOuInexistente.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                binding.activityFormularioDispositivoOacCheckboxOk.isChecked = false
            }
        }
    }

    private fun solicitaPermissaoLocation() {
        try {
            if (ContextCompat.checkSelfPermission(
                            applicationContext,
                            Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                        this,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        101
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()

        if(intent.hasExtra("codAuto")){
            saveSharedPreferences()
        } else {
            sharedPreferences = getPreferences(MODE_PRIVATE)
            codAuto = sharedPreferences.getLong("codAuto", 0L)
        }

        if (codAuto==0L){
            supportActionBar?.title = "Cadastrar Dispositivo (OAC)"
            binding.activityFormularioDispositivoOacFichaReferenciaDispositivo.visibility = View.GONE
            binding.activityFormularioDispositivoOacFoto1.visibility = View.GONE
            binding.activityFormularioDispositivoOacFoto2.visibility = View.GONE
            binding.activityFormularioDispositivoOacBtnCamera.visibility = View.GONE
        } else if (codAuto != 0L) {
            supportActionBar?.title = "Alterar Dispositivo (OAC)"
            tentaPreencherFormulario()
            configuraSpinner()
            binding.activityFormularioDispositivoOacFichaReferenciaDispositivo.text = dispositivo?.fichaReferencia
            binding.activityFormularioDispositivoOacSegmento.isEnabled = false
            verificaFotoAntiga()
        }
        getEstadoDeConservacao()
        binding.activityFormularioDispositivoOacBtnSalvar.setOnClickListener {
            if (isEmpty(binding.activityFormularioDispositivoOacKm.text.toString())) {
                toastPreencherCampoPendente("Preencha o campo Km")
            }
            else if (isEmpty(binding.activityFormularioDispositivoOacSegmento.text.toString())) {
                toastPreencherCampoPendente("Preencha o campo Segmento")
            } else if (isEmpty(binding.activityFormularioDispositivoOacLatitude.text.toString())) {
                toastPreencherCampoPendente("Preencha o campo Latitude")
            } else if (isEmpty(binding.activityFormularioDispositivoOacLongitude.text.toString())) {
                toastPreencherCampoPendente("Preencha o campo Longitude")
            } else if (isEmpty(binding.activityFormularioDispositivoOacExtensao.text.toString())) {
                toastPreencherCampoPendente("Preencha o campo Extensão")
            } else if (isEmpty(binding.activityFormularioDispositivoOacDimensao.text.toString())) {
                toastPreencherCampoPendente("Preencha o campo Dimensao")
            } else if (!binding.activityFormularioDispositivoOacCheckboxOk.isChecked &&
                    !binding.activityFormularioDispositivoOacCheckboxLimpeza.isChecked &&
                    !binding.activityFormularioDispositivoOacCheckboxAssoreado.isChecked &&
                    !binding.activityFormularioDispositivoOacCheckboxAfogado.isChecked &&
                    !binding.activityFormularioDispositivoOacCheckboxErosao.isChecked &&
                    !binding.activityFormularioDispositivoOacCheckboxTestaOuAlaDanificada.isChecked &&
                    !binding.activityFormularioDispositivoOacCheckboxTubulacaoDanificada.isChecked &&
                    !binding.activityFormularioDispositivoOacCheckboxCaixaDanificada.isChecked &&
                    !binding.activityFormularioDispositivoOacCheckboxFissurasOuTrincas.isChecked &&
                    !binding.activityFormularioDispositivoOacCheckboxFissurasOuTrincasDanificada.isChecked &&
                    !binding.activityFormularioDispositivoOacCheckboxTampaDanificadaOuInexistente.isChecked

            ) {
                toastPreencherCampoPendente("Marque pelo menos uma opção de Diagnóstico")
            } else {
                getCaracterKmInteiro()
                setFichaReferencia()
                getEstadoDeConservacao()
                salvarDispositivo()
                getEstadoDeConservacao()
                pegaDispositivoSalvo()
                rolaParaTopoDaPagina()
                binding.activityFormularioDispositivoOacFichaReferenciaDispositivo.text =
                    fichaReferencia
                binding.activityFormularioDispositivoOacFichaReferenciaDispositivo.visibility =
                        View.VISIBLE
                binding.activityFormularioDispositivoOacBtnCamera.visibility = View.VISIBLE
            }
        }
        //configuraSpinner()
    }

    private fun saveSharedPreferences() {
        codAuto = intent.getLongExtra("codAuto", 0L)
        sharedPreferences = getPreferences(MODE_PRIVATE)
        val edit = sharedPreferences.edit()
        edit.putLong("codAuto", codAuto)
        edit.apply()
    }

    private fun verificaFotoAntiga() {
        if (dispositivo?.foto1 == null && dispositivo?.foto2 == null) {
            binding.activityFormularioDispositivoOacFoto1.visibility = View.GONE
            binding.activityFormularioDispositivoOacFoto2.visibility = View.GONE
        }
    }

    private fun rolaParaTopoDaPagina() {
        binding.activityFormularioDispositivoOacScrollview.fullScroll(View.FOCUS_UP)
    }

    private fun pegaDispositivoSalvo() {
        viewModel.getMaxCodAuto().apply {
            dispositivo = viewModel.getDispositiveOac(codAuto)
        }
    }


    private fun toastPreencherCampoPendente(msg: String) {
        val toast = Toast.makeText(this, msg, Toast.LENGTH_LONG)
        toast.show()
    }

    private fun isEmpty(string: String): Boolean {
        return (string == "" || string.trim() == "")
    }

    private fun getLocation(param: String) {
        gpsTracker = GpsTracker(this)
        if (gpsTracker.canGetLocation()) {
            val latitude = gpsTracker.getLatitude()
            val longitude = gpsTracker.getLongitude()

            if (param == "coordenada") {
                binding.activityFormularioDispositivoOacLatitude.setText(latitude.toString())
                binding.activityFormularioDispositivoOacLongitude.setText(longitude.toString())
            }
        } else {
            gpsTracker.showSettingsAlert()
        }
    }

    private fun salvarDispositivo() {
        val disp = Oac(
                codAuto,
                fichaReferencia,
                binding.activityFormularioDispositivoOacMontanteJusante.text.toString(),
                binding.activityFormularioDispositivoOacExtensao.text.toString(),
                binding.activityFormularioDispositivoOacKm.text.toString().substring(0, 7),
                getCaracterKmInteiro(),
                binding.activityFormularioDispositivoOacTipoDeBueiro.text.toString(),
                binding.activityFormularioDispositivoOacEstado.text.toString(),
                binding.activityFormularioDispositivoOacForma.text.toString(),
                binding.activityFormularioDispositivoOacRodovia.text.toString(),
                binding.activityFormularioDispositivoOacDimensao.text.toString(),
                binding.activityFormularioDispositivoOacSentido.text.toString(),
                null,
                binding.activityFormularioDispositivoOacMaterial.text.toString(),
                binding.activityFormularioDispositivoOacTextinputEstadoDeConservacao.text.toString(),
                binding.activityFormularioDispositivoOacLatitude.text.toString(),
                binding.activityFormularioDispositivoOacLongitude.text.toString(),
                binding.activityFormularioDispositivoOacCheckboxOk.isChecked.toString(),
                binding.activityFormularioDispositivoOacCheckboxLimpeza.isChecked.toString(),
                binding.activityFormularioDispositivoOacCheckboxAssoreado.isChecked.toString(),
                binding.activityFormularioDispositivoOacCheckboxAfogado.isChecked.toString(),
                binding.activityFormularioDispositivoOacCheckboxTestaOuAlaDanificada.isChecked.toString(),
                binding.activityFormularioDispositivoOacCheckboxTubulacaoDanificada.isChecked.toString(),
                binding.activityFormularioDispositivoOacCheckboxCaixaDanificada.isChecked.toString(),
                binding.activityFormularioDispositivoOacCheckboxErosao.isChecked.toString(),
                binding.activityFormularioDispositivoOacCheckboxFissurasOuTrincas.isChecked.toString(),
                binding.activityFormularioDispositivoOacCheckboxFissurasOuTrincasDanificada.isChecked.toString(),
                binding.activityFormularioDispositivoOacCheckboxTampaDanificadaOuInexistente.isChecked.toString(),
                DateUtils.getFormattedDate(),
                binding.activityFormularioDispositivoOacObservacao.text.toString(),
                foto1,
                foto2,
                binding.activityFormularioDispositivoOacLote.text.toString(),
                binding.activityFormularioDispositivoOacSegmento.text.toString().toInt(),
                avaliado = (if (dispositivo?.avaliado == Avaliado.PENDENTE.toString()) Avaliado.ABERTO.toString() else Avaliado.OK.toString()),
                "N"
        )
        viewModel.insertDispositiveDs(disp)
        Toast.makeText(
                this@FormularioDispositivoOacActivity,
                "Diagnóstico realizado com sucesso!",
                Toast.LENGTH_LONG
        ).show()
    }

    private fun setFichaReferencia() {
        if (dispositivo == null) {
            val siglaElemento = getSiglaElemento()
            val numRodovia = getNumRodovia()
            val siglaSentido = getSiglaSentido()
            val estado = binding.activityFormularioDispositivoOacEstado.text.toString()
            val km = binding.activityFormularioDispositivoOacKm.text.toString()
            val fichaReferenciaBase = "$siglaElemento $estado $numRodovia $km $siglaSentido"
            val numElementos = viewModel.verifyIdentification(fichaReferenciaBase) + 1

            fichaReferencia =
                    "$siglaElemento $estado $numRodovia $km $siglaSentido $numElementos"

            codAuto = (viewModel.getMaxCodAuto() + 1)
        } else {
            fichaReferencia = dispositivo?.fichaReferencia.toString()
            codAuto = dispositivo?.codAuto!!.toLong()
        }

    }

    private fun getNumRodovia(): String {
        var numRodovia = binding.activityFormularioDispositivoOacRodovia.text.toString()
        numRodovia = numRodovia.substring(numRodovia.length - 3, numRodovia.length)
        return numRodovia
    }

    private fun getCaracterKmInteiro(): String {
        var kmInteiro = binding.activityFormularioDispositivoOacKm.text.toString()
        kmInteiro = kmInteiro.substring(0, 3)
        return kmInteiro
    }

    private fun getSiglaSentido() =
            binding.activityFormularioDispositivoOacSentido.text.toString()[0]

    private fun getSiglaElemento() = "BU"

    private fun tentaPreencherFormulario() {
        val id = sharedPreferences.getLong("codAuto", 0L)
        dispositivo = viewModel.getDispositiveOac(id)
        dispositivo?.let {
            fichaReferencia = it.fichaReferencia
            foto1 = it.foto1
            foto2 = it.foto2
        }

        binding.activityFormularioDispositivoOacKm.setText(dispositivo?.km)
        binding.activityFormularioDispositivoOacEstado.setText(dispositivo?.estado)
        binding.activityFormularioDispositivoOacRodovia.setText(dispositivo?.rodovia)
        binding.activityFormularioDispositivoOacLote.setText(dispositivo?.lote)
        binding.activityFormularioDispositivoOacSentido.setText(dispositivo?.lado)
        binding.activityFormularioDispositivoOacSegmento.setText(dispositivo?.segmento.toString())
        binding.activityFormularioDispositivoOacLatitude.setText(dispositivo?.coordX)
        binding.activityFormularioDispositivoOacLongitude.setText(dispositivo?.coordY)
        binding.activityFormularioDispositivoOacMontanteJusante.setText(dispositivo?.montanteJusante.toString())
        binding.activityFormularioDispositivoOacMaterial.setText(dispositivo?.material)
        binding.activityFormularioDispositivoOacTipoDeBueiro.setText(dispositivo?.tipoDeBueiro)
        binding.activityFormularioDispositivoOacForma.setText(dispositivo?.forma)
        binding.activityFormularioDispositivoOacExtensao.setText(dispositivo?.extensao)
        binding.activityFormularioDispositivoOacDimensao.setText(dispositivo?.dimensao.toString())
        //CheckBoxes
        binding.activityFormularioDispositivoOacCheckboxOk.isChecked = dispositivo!!.ok.toBoolean()
        binding.activityFormularioDispositivoOacCheckboxLimpeza.isChecked =
                dispositivo!!.limpeza.toBoolean()
        binding.activityFormularioDispositivoOacCheckboxTubulacaoDanificada.isChecked =
                dispositivo!!.tubulacaoDanificada.toBoolean()
        binding.activityFormularioDispositivoOacCheckboxAfogado.isChecked =
                dispositivo!!.afogado.toBoolean()
        binding.activityFormularioDispositivoOacCheckboxErosao.isChecked =
                dispositivo!!.erosao.toBoolean()
        binding.activityFormularioDispositivoOacCheckboxTestaOuAlaDanificada.isChecked =
                dispositivo!!.testaOuAlaDanificada.toBoolean()
        binding.activityFormularioDispositivoOacCheckboxAssoreado.isChecked =
                dispositivo!!.assoreado.toBoolean()
        binding.activityFormularioDispositivoOacCheckboxCaixaDanificada.isChecked =
                dispositivo!!.caixaDanificada.toBoolean()
        binding.activityFormularioDispositivoOacCheckboxFissurasOuTrincas.isChecked =
                dispositivo!!.fissurasOuTrinca.toBoolean()
        binding.activityFormularioDispositivoOacCheckboxFissurasOuTrincasDanificada.isChecked =
                dispositivo!!.fissuraOuTrincaDanificada.toBoolean()
        binding.activityFormularioDispositivoOacCheckboxTampaDanificadaOuInexistente.isChecked =
                dispositivo!!.tampaDanificadaOuInexistente.toBoolean()
        binding.activityFormularioDispositivoOacTextinputEstadoDeConservacao.text = dispositivo?.estadoDeConservacao.toString()
        binding.activityFormularioDispositivoOacObservacao.setText(dispositivo?.observacao)

        if (!(dispositivo?.foto1.isNullOrBlank())) {
            if (ImageUtils.readfile(
                    dispositivo?.foto1!!
                ) != null
            ) {
                binding.activityFormularioDispositivoOacFoto1.setImageBitmap(
                    ImageUtils.readfile(
                        dispositivo?.foto1!!
                    )
                )
            }
        }

        if (!(dispositivo?.foto2.isNullOrBlank())) {
            if (ImageUtils.readfile(
                    dispositivo?.foto2!!
                ) != null
            ) {
                binding.activityFormularioDispositivoOacFoto2.setImageBitmap(
                    ImageUtils.readfile(
                        dispositivo?.foto2!!
                    )
                )
            }
        }
    }


    private fun configuraSpinner() {
        configuraSpinnerEstado()
        configuraSpinnerRodovia()
        configuraSpinnerLote()
        configuraSpinnerMontanteJusante()
        configuraSpinnerMaterial()
        configuraSpinnerSentido()
        configuraSpinnerTipoDeBueiro()
        configuraSpinnerForma()
    }

    private fun configuraSpinnerEstado() {
        val estados = resources.getStringArray(R.array.estados)
        val arrayAdapter =
                ArrayAdapter(this@FormularioDispositivoOacActivity, R.layout.dropdown_item, estados)
        binding.activityFormularioDispositivoOacEstado.setAdapter(arrayAdapter)
    }

    private fun configuraSpinnerRodovia() {
        val rodovias = resources.getStringArray(R.array.rodovias)
        val arrayAdapter =
                ArrayAdapter(this@FormularioDispositivoOacActivity, R.layout.dropdown_item, rodovias)
        binding.activityFormularioDispositivoOacRodovia.setAdapter(arrayAdapter)
    }

    private fun configuraSpinnerLote() {
        val lotes = resources.getStringArray(R.array.lotes)
        val arrayAdapter =
                ArrayAdapter(this@FormularioDispositivoOacActivity, R.layout.dropdown_item, lotes)
        binding.activityFormularioDispositivoOacLote.setAdapter(arrayAdapter)
    }

    private fun configuraSpinnerSentido() {
        val sentidos = resources.getStringArray(R.array.sentidos)
        val arrayAdapter =
                ArrayAdapter(this@FormularioDispositivoOacActivity, R.layout.dropdown_item, sentidos)
        binding.activityFormularioDispositivoOacSentido.setAdapter(arrayAdapter)
    }

    private fun configuraSpinnerMontanteJusante() {
        val montanteJusante = resources.getStringArray(R.array.montante_jusante)
        val arrayAdapter =
                ArrayAdapter(this@FormularioDispositivoOacActivity,
                    R.layout.dropdown_item, montanteJusante)
        binding.activityFormularioDispositivoOacMontanteJusante.setAdapter(arrayAdapter)
    }

    private fun configuraSpinnerMaterial() {
        val materiais = resources.getStringArray(R.array.materiais)
        val arrayAdapter =
                ArrayAdapter(this@FormularioDispositivoOacActivity,
                    R.layout.dropdown_item, materiais)
        binding.activityFormularioDispositivoOacMaterial.setAdapter(arrayAdapter)
    }

    private fun configuraSpinnerTipoDeBueiro() {
        val TipoDeBueiro = resources.getStringArray(R.array.tipo_bueiro)
        val arrayAdapter =
                ArrayAdapter(this@FormularioDispositivoOacActivity,
                    R.layout.dropdown_item, TipoDeBueiro)
        binding.activityFormularioDispositivoOacTipoDeBueiro.setAdapter(arrayAdapter)
    }

    private fun configuraSpinnerForma() {
        val formas = resources.getStringArray(R.array.formas)
        val arrayAdapter =
                ArrayAdapter(this@FormularioDispositivoOacActivity, R.layout.dropdown_item, formas)
        binding.activityFormularioDispositivoOacForma.setAdapter(arrayAdapter)
    }

}



