package com.otdengenharia.appotdoac.ui

import com.otdengenharia.appotdoac.R
import com.otdengenharia.appotdoac.databinding.ActivityDownloadOacBinding
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar

class DownloadOacActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDownloadOacBinding

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDownloadOacBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setupToolbar()

        binding.activityDownloadOacBtn.setOnClickListener {
            val segmento = binding.activityDownloadOacTextSegmento.text.toString()

            val intent = Intent(
                this@DownloadOacActivity,
                ListaDispositivosOacActivity::class.java
            ).apply {
                putExtra("segmento", segmento)
            }
            setResult(201, intent)
            finish()
        }
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Download Fotos Referência"
    }
}