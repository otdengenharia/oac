package com.otdengenharia.appotdoac.ui

import com.otdengenharia.appotdoac.R
import com.otdengenharia.appotdoac.databinding.ActivityFiltroOacBinding
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.android.material.chip.Chip

class FiltroOacActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFiltroOacBinding
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFiltroOacBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setupToolbar()
        sharedPreferences = getPreferences(MODE_PRIVATE)

    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Filtros"
    }

    override fun onResume() {
        super.onResume()

        binding.activityFiltroOacBtnVerResultado.setOnClickListener {
            val checkedChipIdsMontanteJusante =
                    getChipList(binding.activityFiltroOacChipGroupMontanteJusante.checkedChipIds)
            val checkedChipsSentido =
                    getChipList(binding.activityFiltroOacChipGroupSentido.checkedChipIds)
            val checkedChipsStatus =
                    getChipList(binding.activityFiltroOacChipGroupStatus.checkedChipIds)

            val km = binding.activityFiltroOacSliderKm.values
            val segmento = binding.activityFiltroOacSliderSegmento.values

            Log.i("km", km.toString())
            Log.i("segmento", segmento.toString())
            Log.i("montante_jusante", checkedChipIdsMontanteJusante.toString())
            Log.i("sentido", checkedChipsSentido.toString())
            Log.i("status", checkedChipsStatus.toString())

            val intent = Intent(
                    this@FiltroOacActivity,
                    ListaDispositivosOacActivity::class.java
            ).apply {
                putStringArrayListExtra("km", km as java.util.ArrayList<String>)
                putStringArrayListExtra("segmento", segmento as java.util.ArrayList<String>)
                putStringArrayListExtra("montante_jusante", checkedChipIdsMontanteJusante as java.util.ArrayList<String>)
                putStringArrayListExtra("sentido", checkedChipsSentido as java.util.ArrayList<String>)
                putStringArrayListExtra("status", checkedChipsStatus as java.util.ArrayList<String>)
            }
            setResult(200, intent)
            finish()
        }
    }

    private fun getChipList(checkedChips: MutableList<Int>): List<String> {
        val listCheckedChipByGroup = mutableListOf<String>()
        checkedChips.forEach {
            val chip = findViewById<Chip>(it)
            listCheckedChipByGroup.add(chip.text.toString())
        }
        return listCheckedChipByGroup
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_limpar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.limpar_filtro -> {
                binding.activityFiltroOacChipGroupMontanteJusante.clearCheck()
                binding.activityFiltroOacChipGroupSentido.clearCheck()
                binding.activityFiltroOacChipGroupStatus.clearCheck()

                if (sharedPreferences.all.isNotEmpty()){
                    val edit = sharedPreferences.edit()
                    edit.clear().apply()
                }
                Toast.makeText(this, "Limpeza de Filtro Concluída.", Toast.LENGTH_LONG).show()

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
