package com.otdengenharia.appotdoac.ui

import com.otdengenharia.appotdoac.R
import com.otdengenharia.appotdoac.api.ApiHelperImpl
import com.otdengenharia.appotdoac.api.RetrofitBuilder
import com.otdengenharia.appotdoac.databinding.ActivityFotoOacBinding
import com.otdengenharia.appotdoac.local.FotoOacViewModel
import com.otdengenharia.appotdoac.local.db.DatabaseBuilder
import com.otdengenharia.appotdoac.local.db.DatabaseHelperImpl
import com.otdengenharia.appotdoac.local.entity.FotoOac
import com.otdengenharia.appotdoac.local.entity.Oac
import com.otdengenharia.appotdoac.ui.recyclerview.adapter.ListaFotosAdapter
import com.otdengenharia.appotdoac.utils.Avaliado
import com.otdengenharia.appotdoac.utils.DateUtils
import com.otdengenharia.appotdoac.utils.ImageUtils
import com.otdengenharia.appotdoac.utils.ViewModelFactory
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import androidx.lifecycle.ViewModelProviders
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream

class FotoOacActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFotoOacBinding
    private lateinit var listaFotosAdapter: ListaFotosAdapter
    private lateinit var viewModel: FotoOacViewModel
    private var dispositivo: Oac? = null
    private var codAuto: Long = 0L
    private var id: Long = 0L
    private var nomeFotoTmp: String? = null
    lateinit var currentPhotoPath: String
    val REQUEST_TAKE_PHOTO = 1
    private val REQUEST_PICK_IMAGE = 2
    lateinit var photoURI: Uri

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFotoOacBinding.inflate(layoutInflater)
        setContentView(binding.root)
        title = "Fotos Dispositivo"

        setupViewModel()
        setupToolbar()

        binding.activityFotoOacBtnCameraAdd.setOnClickListener {
            dispatchTakePictureIntent()
        }

        binding.activityFotoOacBtnGalleryAdd.setOnClickListener {
            openGallery()
        }
    }

    private fun openGallery() {
        Intent(Intent.ACTION_GET_CONTENT).also { intent ->
            intent.type = "image/*"
            intent.resolveActivity(packageManager)?.also {
                startActivityForResult(intent, REQUEST_PICK_IMAGE)
            }
        }
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Visualização Fotos"
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(
                ApiHelperImpl(RetrofitBuilder.apiService),
                DatabaseHelperImpl(DatabaseBuilder.getInstance(applicationContext))
            )
        )[FotoOacViewModel::class.java]
    }

    override fun onResume() {
        super.onResume()

        if (intent.hasExtra("dispositiveOac")) {
            dispositivo = intent.getParcelableExtra("dispositiveOac")
            codAuto = dispositivo!!.codAuto
        }

        verificaFotoAntiga()

        val photosOac = viewModel.getPhotosDispositiveOac(codAuto).also {
            if (it != null) {
                Log.i("ArrayPhoto",it.toString())
                configurarListaFotosAdapter(it)
            }
        }
        val numPhotos = photosOac?.size!!
        nomeFotoTmp =
            dispositivo?.fichaReferencia + "_F" + numPhotos + "_" + dispositivo?.montanteJusante
        binding.activityFotoOacFichaReferenciaDispositivo.text = dispositivo?.fichaReferencia

    }

    private fun configurarListaFotosAdapter(arrayPhotos: List<FotoOac>) {
        if (arrayPhotos.isNotEmpty()) {
            listaFotosAdapter = ListaFotosAdapter(this, arrayPhotos as ArrayList<FotoOac>)
            binding.activityFotsOacListview.adapter = listaFotosAdapter
        }
    }

    private fun verificaFotoAntiga() {
        if (dispositivo?.foto1 == null && dispositivo?.foto2 == null) {
            binding.activityFotoOacFoto1.visibility = View.GONE
            binding.activityFotoOacFoto2.visibility = View.GONE
            binding.activityFotoOacDescricaoAntiga.visibility = View.GONE
        } else {
            if (!dispositivo?.foto1.isNullOrBlank()) {
                if (ImageUtils.readfile(dispositivo!!.foto1!!) != null) {
                    binding.activityFotoOacFoto1.setImageBitmap(
                        ImageUtils.readfile(
                            dispositivo!!.foto1!!
                        )
                    )
                }
            }

            if (!dispositivo?.foto2.isNullOrBlank()) {
                if (ImageUtils.readfile(dispositivo!!.foto2!!) != null) {
                    binding.activityFotoOacFoto2.setImageBitmap(
                        ImageUtils.readfile(
                            dispositivo!!.foto2!!
                        )
                    )
                }
            }
        }
    }

    private fun atualizaStatusDispositivo() {
        dispositivo?.let {
            it.data = DateUtils.getFormattedDate()
            if (it.avaliado == Avaliado.PENDENTE.toString()) {
                it.avaliado = Avaliado.ABERTO.toString()
            } else {
                it.avaliado = Avaliado.OK.toString()
            }
            viewModel.updateDispositiveOac(it)
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val storageDir: File = getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        return File.createTempFile(
            nomeFotoTmp, /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    photoURI = FileProvider.getUriForFile(
                        this,
                        "com.otdengenhariaoac.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            if (photoURI != null) {
//                val imageUri = data?.getData() as Uri
                val imageUri = photoURI
                val imageBitmap: Bitmap

                val bitmap = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    imageBitmap = ImageDecoder.decodeBitmap(
                        ImageDecoder.createSource(
                            contentResolver,
                            imageUri
                        )
                    )
                } else {
                    imageBitmap = MediaStore.Images.Media.getBitmap(contentResolver, imageUri)
                }
                salvarFotoTabela()
                atualizaStatusDispositivo()
                Toast.makeText(this, "Foto adiconada com sucesso!", Toast.LENGTH_LONG).show()
            }
        } else if (requestCode == REQUEST_PICK_IMAGE) {
            Log.i("Log", data?.data.toString())
            var p = ImageUtils.uriToBitmap(data?.data!!, this)
            Log.i("uri", p.toString())

            var dir = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS+"/fotos_galeria_oac")
                    .toURI()
            )

            if (!dir.exists()){
                if(dir.mkdirs()){
                    Log.i("mkdir","OK")
                } else {
                    Log.i("mkdir","NOT OK")
                }
            }

            photoURI = FileProvider.getUriForFile(
                this,
                "com.otdengenhariaoac.fileprovider",
                dir
            )

            var file = File(dir, "$nomeFotoTmp.jpg")

            var f = file.createNewFile()

            try {
                val out = FileOutputStream(file)
                p.compress(Bitmap.CompressFormat.JPEG, 90, out)
                out.flush()
                out.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            currentPhotoPath = "$photoURI/$nomeFotoTmp.jpg"
            photoURI = currentPhotoPath.toUri()
            nomeFotoTmp = "$nomeFotoTmp.jpg"
            salvarFotoTabela()
            atualizaStatusDispositivo()
            Toast.makeText(this, "Foto adiconada com sucesso!", Toast.LENGTH_LONG).show()
        }
    }

    private fun getNomeFotoCompleto(): String {
        var path = currentPhotoPath.split("/")
        return path[path.size - 1]
    }

    private fun salvarFotoTabela() {
        var foto = FotoOac(
            id,
            dispositivo!!.codAuto,
            nomeFotoTmp!!,
            photoURI.toString(),
            observacao = "Obs Teste",
            data = DateUtils.getFormattedDate()
        )
        viewModel.insertPhotoDispositiveOac(foto)
    }

}
