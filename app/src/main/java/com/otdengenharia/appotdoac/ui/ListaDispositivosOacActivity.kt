package com.otdengenharia.appotdoac.ui

import com.otdengenharia.appotdoac.R
import com.otdengenharia.appotdoac.api.ApiHelperImpl
import com.otdengenharia.appotdoac.api.RetrofitBuilder
import com.otdengenharia.appotdoac.databinding.ActivityListaDispositivosOacBinding
import com.otdengenharia.appotdoac.local.ListaDispositivosOacViewModel
import com.otdengenharia.appotdoac.local.db.DatabaseBuilder
import com.otdengenharia.appotdoac.local.db.DatabaseHelperImpl
import com.otdengenharia.appotdoac.local.entity.FotoOac
import com.otdengenharia.appotdoac.local.entity.Oac
import com.otdengenharia.appotdoac.ui.recyclerview.adapter.RecyclerAdapterOac
import com.otdengenharia.appotdoac.utils.Avaliado
import com.otdengenharia.appotdoac.utils.FilterUtils
import com.otdengenharia.appotdoac.utils.Status
import com.otdengenharia.appotdoac.utils.ViewModelFactory
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProviders

class ListaDispositivosOacActivity : AppCompatActivity() {
    private lateinit var binding: ActivityListaDispositivosOacBinding
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var viewModel: ListaDispositivosOacViewModel
    private lateinit var adapter: RecyclerAdapterOac

    private var filtro = false
    private lateinit var listaDispositivosOac: ArrayList<Oac>
    private var result = ArrayList<Oac>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListaDispositivosOacBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        setupToolbar()
        setupViewModel()
        configuraFabButton()
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "OAC"
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

    private fun setupObserver() {
        viewModel.getDispositivesOac().observe(this) { it ->
            Log.i("Observer", "Out When")
            Log.i("Resource", it.status.toString())
            Log.i("Resource Message", it.message.toString())
            Log.i("Resource Data", it.data.toString())

            when (it.status) {
                Status.SUCCESS -> {
                    binding.activityListaDispositivosOacProgressbar.visibility = View.GONE
                    it.data?.let { dispositivesOac ->
                        Log.i("Observer", "OK")
                        listaDispositivosOac = dispositivesOac as ArrayList<Oac>

                        Log.i("Observer", "Filter")
                        sharedPreferences = getPreferences(MODE_PRIVATE)
                        Log.i("sharedPreferences", sharedPreferences.all.toString())

                        if (sharedPreferences.all.isNotEmpty()) {
                            Log.i("sharedPreferences", "OK")
                            val km = sharedPreferences.getString("km", "")?.replace("[", "")
                                ?.replace("]", "")?.split(",")
                            Log.i("km", km.toString())
                            val kmArray = ArrayList<Float>()
                            kmArray.add(km?.first()?.toFloat()!!)
                            kmArray.add(km.last().toFloat())

                            val segmento =
                                sharedPreferences.getString("segmento", "")?.replace("[", "")
                                    ?.replace("]", "")?.split(",")
                            val segmentoArray = ArrayList<Float>()
                            segmentoArray.add(segmento?.first()?.toFloat()!!)
                            segmentoArray.add(segmento.last().toFloat())

                            val montanteJusante =
                                sharedPreferences.getString("montante_jusante", "")?.replace("[", "")
                                    ?.replace("]", "")?.replace(" ", "")?.split(",")
                            val montanteJusanteArray = ArrayList<String>()
                            montanteJusante?.forEach { el ->
                                if (el != "") {
                                    montanteJusanteArray.add(el)
                                }
                            }

                            val sentido =
                                sharedPreferences.getString("sentido", "")?.replace("[", "")
                                    ?.replace("]", "")?.replace(" ", "")?.split(",")
                            val sentidoArray = ArrayList<String>()
                            sentido?.forEach {
                                if (it != "") {
                                    sentidoArray.add(it)
                                }
                            }

                            val status = sharedPreferences.getString("status", "")?.replace("[", "")
                                ?.replace("]", "")?.replace(" ", "")?.split(",")
                            val statusArray = ArrayList<String>()
                            status?.forEach {
                                if (it != "") {
                                    statusArray.add(it)
                                }
                            }

                            result = FilterUtils.filter(
                                listaDispositivosOac,
                                kmArray,
                                segmentoArray,
                                montanteJusanteArray,
                                sentidoArray,
                                statusArray
                            )
                        } else {
                            Log.i("sharedPreferences", "Not OK")
                            result = listaDispositivosOac
                        }
                        configuraRecyclerView(
                            this,
                            result as MutableList<Oac>,
                            viewModel.getAllPhotos() as MutableList<FotoOac>
                        )
                        clicaCardDispositivo()
                        preencheEstatisticasFichas()
                        Toast.makeText(this, "Fichas encontradas: " + result.size, Toast.LENGTH_LONG)
                            .show()
                    }
                    binding.activityListaDispositivosOacRecyclerView.visibility = View.VISIBLE
                }
                Status.LOADING -> {
                    binding.activityListaDispositivosOacProgressbar.visibility = View.VISIBLE
                    binding.activityListaDispositivosOacRecyclerView.visibility = View.GONE
                }
                Status.ERROR -> {
                    //Handle Error
                    binding.activityListaDispositivosOacProgressbar.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }


    private fun configuraRecyclerView(context: Context, lista: MutableList<Oac>, fotos: MutableList<FotoOac>) {
        this.adapter = RecyclerAdapterOac(
                context = context,
                dispositivos = lista,
                fotos = fotos
        )
        binding.activityListaDispositivosOacRecyclerView.adapter = adapter
    }

    private fun configuraFabButton() {

        binding.activityListaDispositivosOacFabAdd.setOnClickListener {
            if (sharedPreferences.all.isNotEmpty()){
                val edit = sharedPreferences.edit()
                edit.clear().apply()
            }
            val intent = Intent(
                    this@ListaDispositivosOacActivity,
                    FormularioDispositivoOacActivity::class.java
            ).apply {
                putExtra("codAuto",0L)
            }
            startActivity(intent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.filter -> {
                val intent = Intent(
                    this@ListaDispositivosOacActivity,
                    FiltroOacActivity::class.java
                )
                startActivityForResult(intent, 200)
                true
            }
            R.id.download -> {
                val intent = Intent(
                    this@ListaDispositivosOacActivity,
                    DownloadOacActivity::class.java
                )
                startActivityForResult(intent, 201)
                true
            }
            R.id.upload -> {
                viewModel.uploadImagesAndDispositives(this)
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 200) {
            filtro = true
            val kmArray = data?.getStringArrayListExtra("km") as ArrayList<Float>
            val segmentoArray = data.getStringArrayListExtra("segmento") as ArrayList<Float>
            val montanteJusanteArray = data.getStringArrayListExtra("montante_jusante")
            val sentidoArray = data.getStringArrayListExtra("sentido")
            val statusArray = data.getStringArrayListExtra("status")

            saveSharedPreferences(kmArray, segmentoArray, montanteJusanteArray, sentidoArray, statusArray)
            Log.i("kmArray", kmArray.toString())
            Log.i("kmArray[0]", kmArray.first().toString())

            result = FilterUtils.filter(
                dispositivos = listaDispositivosOac,
                kmArray,
                segmentoArray,
                montanteJusanteArray!!,
                sentidoArray!!,
                statusArray!!
            )
            Log.i("disp", result.toString())

            Toast.makeText(this, "Fichas encontradas: " + result.size, Toast.LENGTH_LONG).show()
        } else if (resultCode == 201) {
            val segmento = data?.getStringExtra("segmento")

            if (segmento != null) {
                viewModel.setSegmento(this, segmento.toInt())
            }
        }
    }

    private fun saveSharedPreferences(
        kmArray: List<Float>,
        segmentoArray: List<Float>,
        montanteJusanteArray: List<String>?,
        sentidoArray: List<String>?,
        statusArray: List<String>?
    ) {
        sharedPreferences = getPreferences(MODE_PRIVATE)
        val edit = sharedPreferences.edit()
        edit.putString("km", kmArray.toString())
        edit.putString("segmento", segmentoArray.toString())
        edit.putString("montante_jusante", montanteJusanteArray.toString())
        edit.putString("sentido", sentidoArray.toString())
        edit.putString("status", statusArray.toString())
        edit.apply()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onResume() {
        super.onResume()
        setupViewModel()
        setupObserver()
        setupObserverUpload()
        setupObserverDownload()


        Log.i("onResume", "OK")

        configuraRecyclerView(
            this,
            result as MutableList<Oac>,
            viewModel.getAllPhotos() as MutableList<FotoOac>
        )
        clicaCardDispositivo()
        preencheEstatisticasFichas()

        findViewById<SearchView>(R.id.activity_lista_dispositivos_oac_search).setOnQueryTextListener(
            object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    adapter.getFilter()?.filter(query)
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    adapter.getFilter()?.filter(newText)
                    return false
                }

            })
    }

    private fun clicaCardDispositivo() {
        adapter.setOnItemClickListener(object :
                RecyclerAdapterOac.OnItemClickListener {
            override fun onItemClick(v: View, position: Int) {
                val intent = Intent(
                        this@ListaDispositivosOacActivity,
                        FormularioDispositivoOacActivity::class.java
                ).apply {
                    putExtra(
                        "codAuto",
                        adapter.getDispositives()[position].codAuto
                    )
                }
                startActivity(intent)
            }
        })
    }

    private fun preencheEstatisticasFichas() {
        val statistics = viewModel.statistics()
        binding.activityListaDispositivosOacAcumuladoTotalValor.text =
            statistics[Avaliado.OK.toString()].toString()
        binding.activityListaDispositivosOacAcumuladoDiarioValor.text =
            statistics["OK_DAY"].toString()
        binding.activityListaDispositivosOacEmAbertoValor.text =
            statistics[Avaliado.ABERTO.toString()].toString()
        binding.activityListaDispositivosOacPendenteValor.text =
            (statistics[Avaliado.PENDENTE.toString()]?.plus(statistics[Avaliado.ABERTO.toString()]!!)).toString()

        Log.i("statistics", statistics.toString())
    }

    private fun setupObserverUpload() {
        viewModel.getResponsedispositivesOac().observe(this) {
            Log.i("Observer", "Out When")
            Log.i("Resource", it.status.toString())
            Log.i("Resource Message", it.message.toString())
            Log.i("Resource Data", it.data.toString())

            when (it.status) {
                Status.SUCCESS -> {
                    binding.activityListaDispositivosOacProgressbar.visibility = View.GONE
                    binding.activityListaDispositivosOacRecyclerView.visibility = View.VISIBLE
                    Toast.makeText(this, "Upload Concluído.", Toast.LENGTH_LONG).show()
                }
                Status.LOADING -> {
                    binding.activityListaDispositivosOacProgressbar.visibility = View.VISIBLE
                    binding.activityListaDispositivosOacRecyclerView.visibility = View.GONE
                    Toast.makeText(this, "Upload em andamento.", Toast.LENGTH_LONG).show()
                }
                Status.ERROR -> {
                    //Handle Error
                    binding.activityListaDispositivosOacProgressbar.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun setupObserverDownload() {
        viewModel.getPhotoDispositivesOac().observe(this) {
            Log.i("Observer Download", "Out When")
            Log.i("Resource", it.status.toString())
            Log.i("Resource Message", it.message.toString())
            Log.i("Resource Data", it.data.toString())

            when (it.status) {
                Status.SUCCESS -> {
                    binding.activityListaDispositivosOacProgressbar.visibility = View.GONE
                    binding.activityListaDispositivosOacRecyclerView.visibility = View.VISIBLE
                    Toast.makeText(this, "Download Concluído.", Toast.LENGTH_LONG).show()
                }
                Status.LOADING -> {
                    binding.activityListaDispositivosOacProgressbar.visibility = View.VISIBLE
                    binding.activityListaDispositivosOacRecyclerView.visibility = View.GONE
                    Toast.makeText(this, "Download em andamento.", Toast.LENGTH_LONG).show()
                }
                Status.ERROR -> {
                    //Handle Error
                    binding.activityListaDispositivosOacProgressbar.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(
                ApiHelperImpl(RetrofitBuilder.apiService),
                DatabaseHelperImpl(DatabaseBuilder.getInstance(applicationContext))
            )
        )[ListaDispositivosOacViewModel::class.java]
    }

}