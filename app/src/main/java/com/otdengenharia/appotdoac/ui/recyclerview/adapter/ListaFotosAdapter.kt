package com.otdengenharia.appotdoac.ui.recyclerview.adapter

import com.otdengenharia.appotdoac.R
import com.otdengenharia.appotdoac.local.entity.FotoOac
import com.otdengenharia.appotdoac.utils.ImageUtils
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri

class ListaFotosAdapter(private val context: Context, private val fotoOacArrayList: ArrayList<FotoOac>) : BaseAdapter() {

    override fun getViewTypeCount(): Int {
        return count
    }

    override fun getItemViewType(position: Int): Int {

        return position
    }

    override fun getCount(): Int {
        return fotoOacArrayList.size
    }

    override fun getItem(position: Int): Any {
        return fotoOacArrayList[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val holder: ViewHolder

        if (convertView == null) {
            holder = ViewHolder()
            val inflater = context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.foto_oac_listview_item, null, true)

            holder.tvname = convertView!!.findViewById(R.id.foto_oac_listview_item_nome) as TextView
            holder.iv = convertView.findViewById(R.id.foto_oac_listview_item_imagem) as ImageView

            convertView.tag = holder
        } else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = convertView.tag as ViewHolder
        }

        holder.tvname!!.text = fotoOacArrayList[position].nome
        val bitmap = ImageUtils.uriToBitmap(fotoOacArrayList[position].path!!.toUri(),context)
        holder.iv!!.setImageBitmap(bitmap)

        return convertView
    }

    private inner class ViewHolder {

        var tvname: TextView? = null
        var iv: ImageView? = null

    }

}