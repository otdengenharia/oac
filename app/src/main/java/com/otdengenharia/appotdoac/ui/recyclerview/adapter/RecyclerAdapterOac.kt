package com.otdengenharia.appotdoac.ui.recyclerview.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.otdengenharia.appotdoac.R
import com.otdengenharia.appotdoac.local.entity.FotoOac
import com.otdengenharia.appotdoac.local.entity.Oac
import com.otdengenharia.appotdoac.utils.ImageUtils
import java.util.*

class RecyclerAdapterOac(
    private val context: Context,
    private var dispositivos: MutableList<Oac> = mutableListOf(),
    private var fotos: MutableList<FotoOac> = mutableListOf()
) : RecyclerView.Adapter<RecyclerAdapterOac.ViewHolder>() {
    private lateinit var listener: OnItemClickListener
    private var copyList: MutableList<Oac> = dispositivos

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerAdapterOac.ViewHolder {
        val viewCriada = LayoutInflater.from(context)
            .inflate(R.layout.card_layout_dispositivos_oac, parent, false)
        return ViewHolder(viewCriada)
    }

    override fun onBindViewHolder(
        holder: RecyclerAdapterOac.ViewHolder,
        position: Int
    ) {
        val dispositivo = dispositivos[position]
        holder.bind(dispositivo)
    }

    override fun getItemCount(): Int {
        return dispositivos.size
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        this.listener = listener
    }

    fun add(dispositivos: List<Oac>) {
        this.dispositivos.addAll(dispositivos)
        notifyItemRangeChanged(0, dispositivos.size)
    }

    fun getDispositives(): MutableList<Oac> {
        return this.dispositivos
    }

    fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence): FilterResults {
                val charSequenceString = constraint.toString()
                if (charSequenceString.isNotEmpty()) {
                    val filteredAuxList: MutableList<Oac> = ArrayList()
                    for (disp in dispositivos) {
                        if (disp.fichaReferencia.lowercase(Locale.getDefault())
                                .contains(charSequenceString.lowercase(Locale.getDefault()))
                        ) {
                            filteredAuxList.add(disp)
                        }
                        dispositivos = filteredAuxList
                    }
                } else {
                    dispositivos = copyList
                }
                val results = FilterResults()
                results.values = dispositivos
                return results
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults) {
                dispositivos = results.values as MutableList<Oac>
                notifyDataSetChanged()
            }
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        private val fichaReferencia =
            itemView.findViewById<TextView>(R.id.card_layout_dispositivos_oac_id)
        private val montanteJusante =
            itemView.findViewById<TextView>(R.id.card_layout_dispositivos_oac_montante_jusante)
        private val km = itemView.findViewById<TextView>(R.id.card_layout_dispositivos_oac_km)
        private val lado =
            itemView.findViewById<TextView>(R.id.card_layout_dispositivos_oac_sentido)
        private val segmento =
            itemView.findViewById<TextView>(R.id.card_layout_dispositivos_oac_segmento)
        private val foto1 = itemView.findViewById<ImageView>(R.id.card_layout_dispositivos_oac_foto)
        private val foto2 =
            itemView.findViewById<ImageView>(R.id.card_layout_dispositivos_oac_foto2)
        private val status =
            itemView.findViewById<TextView>(R.id.card_layout_dispositivos_oac_status)


        fun bind(dispositivo: Oac) {
            fichaReferencia.text = dispositivo.fichaReferencia
            montanteJusante.text = dispositivo.montanteJusante
            km.text = dispositivo.km
            lado.text = dispositivo.lado
            segmento.text = dispositivo.segmento.toString()
            status.text = dispositivo.avaliado

            // Tipo cor - Status do Card
            if (dispositivo.avaliado == "OK") {
                status.setTextColor(Color.rgb(0, 153, 0))
            } else if (dispositivo.avaliado == "ABERTO") {
                status.setTextColor(Color.rgb(255, 128, 0))
            } else {
                status.setTextColor(Color.RED)
            }

            val fotos = fotos.filter {
                it.ficha == dispositivo.codAuto
            }

            if (dispositivo.foto1 == null) run {
                if (fotos.isNotEmpty()) {
                    fotos[0].let {
                        val bitmap = ImageUtils.uriToBitmap(it.path!!.toUri(), context)
                        bitmap.apply {
                            foto1.setImageBitmap(bitmap)
                        }
                    }
                }
            }

            if (dispositivo.foto2 == null) run {
                if (fotos.isNotEmpty() && fotos.size > 1) {
                    fotos[1].let {
                        val bitmap = ImageUtils.uriToBitmap(it.path!!.toUri(), context)
                        bitmap.apply {
                            foto2.setImageBitmap(bitmap)
                        }
                    }
                }
            }

            if (dispositivo.foto1 != null && dispositivo.foto2 != null) {
                val filename1 = dispositivo.foto1!!
                val filename2 = dispositivo.foto2!!

                val bitmap1 = ImageUtils.readfile(filename1)
                bitmap1.apply {
                    foto1.setImageBitmap(bitmap1)
                }

                val bitmap2 = ImageUtils.readfile(filename2)
                bitmap2.apply {
                    foto2.setImageBitmap(bitmap2)
                }

            }
        }

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            if (v != null) {
                listener.onItemClick(v, adapterPosition)
            }
        }

    }

    interface OnItemClickListener {
        fun onItemClick(v: View, position: Int)
    }

}