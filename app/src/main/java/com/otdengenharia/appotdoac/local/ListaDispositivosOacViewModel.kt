package com.otdengenharia.appotdoac.local

import com.otdengenharia.appotdoac.api.ApiHelper
import com.otdengenharia.appotdoac.local.db.DatabaseHelper
import com.otdengenharia.appotdoac.local.entity.FotoOac
import com.otdengenharia.appotdoac.local.entity.Oac
import com.otdengenharia.appotdoac.model.ApiOac
import com.otdengenharia.appotdoac.utils.Avaliado
import com.otdengenharia.appotdoac.utils.DateUtils
import com.otdengenharia.appotdoac.utils.ImageUtils
import com.otdengenharia.appotdoac.utils.Resource
import android.content.ContentValues
import android.content.Context
import android.os.Build
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.util.ArrayMap
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.net.toUri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.GsonBuilder

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType
import okhttp3.RequestBody
import java.net.URL
import kotlin.properties.Delegates


class ListaDispositivosOacViewModel(
    private val apiHelper: ApiHelper,
    private val dbHelper: DatabaseHelper
) :
    ViewModel() {

    private val dispositivesOac = MutableLiveData<Resource<List<Oac>>>()
    private val responsePost = MutableLiveData<Resource<List<Oac>>>()
    private val path: String = "https://otdengenharia.com/api/api/oac/fotosAntigas/050/"
    private val photosDs = MutableLiveData<Resource<List<String>>>()
    private var segmento by Delegates.notNull<Int>()


    init {
        fetchdispositivesOac()
    }

    private fun fetchdispositivesOac() {
        viewModelScope.launch {
            Log.i("CoroutineScope", "OK")
            dispositivesOac.postValue(Resource.loading(null))

            try {

                val dispositivesOacFromDb = dbHelper.getDispositivesOac()
                Log.i("dispositivesFromDb", dispositivesOacFromDb.size.toString())

                if (dispositivesOacFromDb.isEmpty()) {
                    Log.i("ifEmpty", "OK")

                    val dispositivesOacFromApi = apiHelper.getDispositivesOac()
                    Log.i("dispositivesFromAPI", apiHelper.getDispositivesOac().size.toString())

                    val dispositivesOacToInsertInDB = mutableListOf<Oac>()

                    for (apiDispositiveOac in dispositivesOacFromApi) {
                        val dispositiveOac = Oac(
                            apiDispositiveOac.codAuto,
                            apiDispositiveOac.fichaReferencia,
                            apiDispositiveOac.montanteJusante,
                            apiDispositiveOac.extensao,
                            apiDispositiveOac.km,
                            apiDispositiveOac.kmInteiro,
                            apiDispositiveOac.tipoDeBueiro,
                            apiDispositiveOac.estado,
                            apiDispositiveOac.forma,
                            apiDispositiveOac.rodovia,
                            apiDispositiveOac.dimensao,
                            apiDispositiveOac.lado,
                            apiDispositiveOac.estruturaDeSaida,
                            apiDispositiveOac.material,
                            apiDispositiveOac.estadoDeConservacao,
                            apiDispositiveOac.coordX,
                            apiDispositiveOac.coordY,
                            apiDispositiveOac.ok,
                            apiDispositiveOac.limpeza,
                            apiDispositiveOac.assoreado,
                            apiDispositiveOac.afogado,
                            apiDispositiveOac.testaOuAlaDanificada,
                            apiDispositiveOac.tubulacaoDanificada,
                            apiDispositiveOac.caixaDanificada,
                            apiDispositiveOac.erosao,
                            apiDispositiveOac.fissurasOuTrinca,
                            apiDispositiveOac.fissuraOuTrincaDanificada,
                            apiDispositiveOac.tampaDanificadaOuInexistente,
                            apiDispositiveOac.data,
                            apiDispositiveOac.observacao,
                            apiDispositiveOac.foto1,
                            apiDispositiveOac.foto2,
                            apiDispositiveOac.lote,
                            apiDispositiveOac.segmento,
                            apiDispositiveOac.avaliado,
                            apiDispositiveOac.editadoEm
                        )
                        dispositivesOacToInsertInDB.add(dispositiveOac)
                    }

                    dbHelper.insertAllDispositivesOac(dispositivesOacToInsertInDB)

                    dispositivesOac.postValue(Resource.success(dispositivesOacToInsertInDB))

                } else {
                    Log.i("fromDB", "OK")
                    dispositivesOac.postValue(Resource.success(dispositivesOacFromDb))
                }
            } catch (e: Exception) {
                dispositivesOac.postValue(Resource.error("Something Went Wrong", null))
            }
        }
    }

    fun statistics(): ArrayMap<String, Int> {
        val statistics: ArrayMap<String, Int> = ArrayMap()

        statistics[Avaliado.OK.toString()] = dbHelper.countDispositivesOac(Avaliado.OK.toString())
        statistics["OK_DAY"] = dbHelper.countDispositivesOacByDate(
            Avaliado.OK.toString(),
            DateUtils.getFormattedDate()
        )
        statistics[Avaliado.ABERTO.toString()] =
            dbHelper.countDispositivesOac(Avaliado.ABERTO.toString())
        statistics[Avaliado.PENDENTE.toString()] =
            dbHelper.countDispositivesOac(Avaliado.PENDENTE.toString())

        return statistics
    }

    fun getDispositivesOac(): LiveData<Resource<List<Oac>>> {
        return dispositivesOac
    }

    fun insertDispositiveDs(dispositive: Oac) {
        viewModelScope.launch {
            dbHelper.insertDispositivesOac(dispositive)
        }
    }

    fun getDispositivesOacByDate(date: String): List<Oac> {
        var list: List<Oac>? = null
        viewModelScope.launch {
            list = dbHelper.findByDate(date)
        }
        return list!!
    }

    fun sendDispositivesOac() {

        viewModelScope.launch {

            val findByDate = dbHelper.findByDate(DateUtils.getFormattedDate())
            Log.i("findByDate", findByDate.toString())
            val dispositivesOacApi = mutableListOf<ApiOac>()
            responsePost.postValue(Resource.loading(null))
            findByDate.forEach {
                val dispositive = ApiOac(
                    it.codAuto,
                    it.fichaReferencia,
                    it.montanteJusante,
                    it.extensao,
                    it.km,
                    it.kmInteiro,
                    it.tipoDeBueiro,
                    it.estado,
                    it.forma,
                    it.rodovia,
                    it.dimensao,
                    it.lado,
                    it.estruturaDeSaida,
                    it.material,
                    it.estadoDeConservacao,
                    it.coordX,
                    it.coordY,
                    it.ok,
                    it.limpeza,
                    it.assoreado,
                    it.afogado,
                    it.testaOuAlaDanificada,
                    it.tubulacaoDanificada,
                    it.caixaDanificada,
                    it.erosao,
                    it.fissurasOuTrinca,
                    it.fissuraOuTrincaDanificada,
                    it.tampaDanificadaOuInexistente,
                    it.data,
                    it.observacao,
                    it.foto1,
                    it.foto2,
                    it.lote,
                    it.segmento,
                    it.avaliado,
                    it.editadoEm
                )

                if (dispositive.editadoEm == "N") {
                    dispositivesOacApi.add(dispositive)
                }
            }

            val gson = GsonBuilder().create()
            val myCustomArray = gson.toJsonTree(dispositivesOacApi).asJsonArray

            Log.i("gsonArray", myCustomArray.toString())

            val requestBody =
                RequestBody.create(
                    MediaType.parse("application/json; charset=utf-8"), myCustomArray.toString()
                )


            Log.i("requestBody", requestBody.toString())
            val sendDispositiveOac = apiHelper.sendDispositiveOac(requestBody)
            Log.i("Response", sendDispositiveOac.toString())

            if (sendDispositiveOac.code() == 200) {
                dispositivesOacApi.forEach {
                    dbHelper.insertDispositivesOac(
                        Oac(
                            it.codAuto,
                            it.fichaReferencia,
                            it.montanteJusante,
                            it.extensao,
                            it.km,
                            it.kmInteiro,
                            it.tipoDeBueiro,
                            it.estado,
                            it.forma,
                            it.rodovia,
                            it.dimensao,
                            it.lado,
                            it.estruturaDeSaida,
                            it.material,
                            it.estadoDeConservacao,
                            it.coordX,
                            it.coordY,
                            it.ok,
                            it.limpeza,
                            it.assoreado,
                            it.afogado,
                            it.testaOuAlaDanificada,
                            it.tubulacaoDanificada,
                            it.caixaDanificada,
                            it.erosao,
                            it.fissurasOuTrinca,
                            it.fissuraOuTrincaDanificada,
                            it.tampaDanificadaOuInexistente,
                            it.data,
                            it.observacao,
                            it.foto1,
                            it.foto2,
                            it.lote,
                            it.segmento,
                            it.avaliado,
                            "S"
                        )
                    )

                }
                responsePost.postValue(Resource.success(null))
            } else {
                responsePost.postValue(Resource.error("Erro: Upload não concluído", null))
            }
        }
    }

    fun uploadImages(context: Context) {
        viewModelScope.launch {
//            var photos = dbHelper.getPhotosdispositivesOac().filter { it.sincronizado != "S" }
            val photos = dbHelper.getPhotosDispositivesOac()
            Log.i("photos", photos.toString())

            photos.forEach {
                val bitmap = ImageUtils.uriToBitmap(it.path!!.toUri(), context)
                //Log.i("bitmap", bitmap.toString())
                val image = ImageUtils.convertToString(bitmap)
                //Log.i("image",image.toString())

                val response = apiHelper.uploadImage(it.nome, image!!)
                Log.i("response", response.toString())
            }
        }
    }

    fun uploadImagesAndDispositives(context: Context) {
        sendDispositivesOac()
        uploadImages(context)
    }

    fun getAllPhotos(): List<FotoOac> {
        var listPhotos: List<FotoOac>? = null
        viewModelScope.launch {
            listPhotos = dbHelper.getPhotosDispositivesOac()
        }
        return listPhotos!!
    }

    fun getResponsedispositivesOac(): LiveData<Resource<List<Oac>>> {
        return responsePost
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private suspend fun fetchPhotosDispositivesOac(context: Context) {
        viewModelScope.launch {
            Log.i("CoroutineScope Download", "OK")
            photosDs.postValue(Resource.loading(null))
            try {
                dbHelper.findBySegmento(segmento).forEach { imageBase64 ->
                    val photo1Renamed =
                        imageBase64.foto1?.replace(" ", "%20")
                    val photo2Renamed =
                        imageBase64.foto2?.replace(" ", "%20")

                    imageBase64.foto1.toString()
                        .let {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                                if (!imageBase64.foto1.isNullOrBlank()) {
                                    downloadFile(context, path + photo1Renamed, it)
                                }
                            }
                        }
                    imageBase64.foto2.toString()
                        .let {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                                if (!imageBase64.foto2.isNullOrBlank()) {
                                    downloadFile(context, path + photo2Renamed, it)
                                }
                            }
                        }
                }
                photosDs.postValue(Resource.success(null))
            } catch (e: Exception) {
                photosDs.postValue(Resource.error("Something Went Wrong", null))
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private suspend fun downloadFile(context: Context, url: String, fileName: String) {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        

        withContext(Dispatchers.IO) {
            val contentValues = ContentValues().apply {
                put(MediaStore.MediaColumns.DISPLAY_NAME, fileName)
                put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg")
                put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_DOWNLOADS)
            }
            val resolver = context.contentResolver
            val uri = resolver.insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, contentValues)
            if (uri != null) {
                URL(url).openStream().use { input ->
                    resolver.openOutputStream(uri).use { output ->
                        input.copyTo(output!!, DEFAULT_BUFFER_SIZE)
                    }
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @JvmName("setSegmento1")
    fun setSegmento(context: Context, segmento: Int) {
        this.segmento = segmento
        viewModelScope.launch {
            fetchPhotosDispositivesOac(context)
        }
    }

    fun getPhotoDispositivesOac(): LiveData<Resource<List<String>>> {
        return photosDs
    }

}