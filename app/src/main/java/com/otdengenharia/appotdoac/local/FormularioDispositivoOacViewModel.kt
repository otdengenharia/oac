package com.otdengenharia.appotdoac.local

import com.otdengenharia.appotdoac.local.db.DatabaseHelper
import com.otdengenharia.appotdoac.local.entity.Oac
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class FormularioDispositivoOacViewModel(
    private val dbHelper: DatabaseHelper
)  : ViewModel()  {

    fun insertDispositiveDs(dispositive: Oac) {
        viewModelScope.launch {
            dbHelper.insertDispositivesOac(dispositive)
        }
    }

    fun verifyIdentification(identification : String) : Int {
        var numIdentification = 0
        viewModelScope.launch {
            numIdentification = dbHelper.verifyId(identification)
        }
        return numIdentification
    }

    fun getMaxCodAuto() : Long {
        var maxCodAuto = 0
        viewModelScope.launch {
            maxCodAuto = dbHelper.getMaxCodAuto().toInt()
        }
        return maxCodAuto.toLong()
    }

    fun getDispositiveOac(codAuto : Long) : Oac {
        var disp : Oac? = null
        viewModelScope.launch {
            disp = dbHelper.getDispositiveOac(codAuto)
        }
        return disp!!
    }

    fun updateDispositiveDs(disp: Oac) {
        viewModelScope.launch {
            dbHelper.updateDispositiveOac(disp)
        }
    }
}