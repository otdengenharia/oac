package com.otdengenharia.appotdoac.local.dao

import com.otdengenharia.appotdoac.local.entity.FotoOac
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface FotoOacDao {
    @Query("SELECT * FROM FotoOac")
    fun all(): List<FotoOac>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(fotoDs: FotoOac)

    @Query("SELECT * FROM FotoOac WHERE ficha = :codAuto")
    fun findByCodAuto(codAuto: Long): List<FotoOac>

    @Query("SELECT * FROM FotoOac WHERE id = :id")
    fun findById(id: Long): FotoOac

    @Query("SELECT * FROM FotoOac WHERE sincronizado = 'N'")
    fun findNotSync(): List<FotoOac>

    @Query("SELECT COUNT(*) FROM FotoOac WHERE ficha = :ficha")
    fun countByFicha(ficha: Long): Int

    @Query("SELECT MAX(id) FROM FotoOac")
    fun getMaxCodAuto(): Long

}