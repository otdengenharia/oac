package com.otdengenharia.appotdoac.local

import com.otdengenharia.appotdoac.local.db.DatabaseHelper
import com.otdengenharia.appotdoac.local.entity.FotoOac
import com.otdengenharia.appotdoac.local.entity.Oac
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class FotoOacViewModel(
    private val dbHelper: DatabaseHelper
) : ViewModel() {

    fun updateDispositiveOac(dispositive: Oac) {
        viewModelScope.launch {
            dbHelper.insertDispositivesOac(dispositive)
        }
    }

    fun insertPhotoDispositiveOac(photo: FotoOac) {
        viewModelScope.launch {
            dbHelper.insertPhotosDispositivesOac(photo)
        }
    }

    fun getPhotosDispositiveOac(codAuto : Long) : List<FotoOac>? {
        var photos : List<FotoOac>? = null
        viewModelScope.launch {
           photos = dbHelper.getPhotosDispositivesOacByCodAuto(codAuto)
        }
        return photos
    }
}