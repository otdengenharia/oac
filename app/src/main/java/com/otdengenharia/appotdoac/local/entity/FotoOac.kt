package com.otdengenharia.appotdoac.local.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity
data class FotoOac(
        @PrimaryKey(autoGenerate = true)
        var id: Long = 0L,
        var ficha: Long,
        var nome: String,
        var path: String?,
        var observacao: String,
        var data: String,
        var sincronizado: String? = "N"
) : Parcelable
