package com.otdengenharia.appotdoac.local.dao

import com.otdengenharia.appotdoac.local.entity.Oac
import androidx.room.*

@Dao
interface OacDao {

    @Query("SELECT * FROM Oac")
    fun all(): List<Oac>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(dispositivo: Oac)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addAll(dispositivos: List<Oac>)

    @Update
    fun update(dispositive : Oac)

    @Query("SELECT * FROM Oac WHERE codAuto = :codAuto")
    fun findByCodAuto(codAuto: Long): Oac

    @Query("SELECT COUNT(avaliado) FROM Oac WHERE avaliado = :state")
    fun count(state: String): Int

    @Query("SELECT COUNT(avaliado) FROM Oac WHERE avaliado = :state AND data = :data")
    fun countByDate(state: String, data: String): Int

    @Query("SELECT COUNT(*) FROM Oac WHERE fichaReferencia LIKE '%' || :id || '%'")
    fun verifyId(id: String): Int

    @Query("SELECT MAX(codAuto) FROM Oac")
    fun getMaxCodAuto(): Long

    @Query("SELECT * FROM OAC WHERE data = :date")
    fun findByDate(date: String): List<Oac>

    @Query("SELECT * FROM Oac WHERE segmento = :segmento")
    fun findBySegmento(segmento: Int): List<Oac>
}