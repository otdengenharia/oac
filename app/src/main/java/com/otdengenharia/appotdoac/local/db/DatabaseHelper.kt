package com.otdengenharia.appotdoac.local.db

import com.otdengenharia.appotdoac.local.entity.FotoOac
import com.otdengenharia.appotdoac.local.entity.Oac

interface DatabaseHelper {
    suspend fun getDispositivesOac() : List<Oac>

    suspend fun getDispositiveOac(codAuto : Long) : Oac

    suspend fun insertAllDispositivesOac(dispositives : List<Oac>)

    suspend fun insertDispositivesOac(dispositive : Oac)

    suspend fun updateDispositiveOac(dispositive : Oac)

    fun countDispositivesOac(state : String) : Int

    suspend fun findByDate(date: String): List<Oac>

    fun countDispositivesOacByDate(state : String, date : String) : Int

    suspend fun verifyId(id: String): Int

    suspend fun getMaxCodAuto() : Long

    suspend fun getPhotosDispositivesOac() : List<FotoOac>

    suspend fun insertPhotosDispositivesOac(photos : FotoOac)

    suspend fun getPhotosDispositivesOacByCodAuto(codAuto: Long): List<FotoOac>

    suspend fun findBySegmento(segmento: Int): List<Oac>
}