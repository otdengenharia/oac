package com.otdengenharia.appotdoac.local.db

import com.otdengenharia.appotdoac.local.dao.FotoOacDao
import com.otdengenharia.appotdoac.local.dao.OacDao
import com.otdengenharia.appotdoac.local.entity.FotoOac
import com.otdengenharia.appotdoac.local.entity.Oac
import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Oac::class, FotoOac::class], version = 11)
abstract class AppDatabase : RoomDatabase(){
    abstract fun oacDao() : OacDao
    abstract fun fotoOacDao() : FotoOacDao
}