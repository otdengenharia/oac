package com.otdengenharia.appotdoac.local.db

import com.otdengenharia.appotdoac.local.entity.FotoOac
import com.otdengenharia.appotdoac.local.entity.Oac

class DatabaseHelperImpl(private val appDatabase: AppDatabase) : DatabaseHelper {
    override suspend fun getDispositivesOac(): List<Oac> {
        return appDatabase.oacDao().all()
    }

    override suspend fun getDispositiveOac(codAuto : Long): Oac {
        return appDatabase.oacDao().findByCodAuto(codAuto)
    }

    override suspend fun insertAllDispositivesOac(dispositives: List<Oac>) {
        return appDatabase.oacDao().addAll(dispositives)
    }


    override suspend fun insertDispositivesOac(dispositive: Oac) {
        return appDatabase.oacDao().add(dispositive)
    }

    override suspend fun updateDispositiveOac(dispositive: Oac) {
        return appDatabase.oacDao().update(dispositive)
    }

    override fun countDispositivesOac(state: String): Int {
        return appDatabase.oacDao().count(state)
    }

    override suspend fun findByDate(date: String): List<Oac> {
        return appDatabase.oacDao().findByDate(date)
    }

    override fun countDispositivesOacByDate(state: String, date: String): Int {
        return appDatabase.oacDao().countByDate(state,date)
    }

    override suspend fun verifyId(id: String): Int {
        return appDatabase.oacDao().verifyId(id)
    }

    override suspend fun getMaxCodAuto(): Long {
        return appDatabase.oacDao().getMaxCodAuto()
    }

    override suspend fun getPhotosDispositivesOac(): List<FotoOac> {
        return appDatabase.fotoOacDao().all()
    }

    override suspend fun insertPhotosDispositivesOac(photos: FotoOac) {
        return appDatabase.fotoOacDao().add(photos)
    }

    override suspend fun getPhotosDispositivesOacByCodAuto(codAuto: Long): List<FotoOac> {
        return appDatabase.fotoOacDao().findByCodAuto(codAuto)
    }

    override suspend fun findBySegmento(segmento: Int): List<Oac> {
        return  appDatabase.oacDao().findBySegmento(segmento)
    }
}