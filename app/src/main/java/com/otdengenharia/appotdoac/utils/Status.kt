package com.otdengenharia.appotdoac.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}