package com.otdengenharia.appotdoac.utils

enum class Avaliado {
    OK, ABERTO, PENDENTE
}
