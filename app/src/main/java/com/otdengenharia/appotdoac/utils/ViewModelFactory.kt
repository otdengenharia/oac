package com.otdengenharia.appotdoac.utils

import com.otdengenharia.appotdoac.api.ApiHelper
import com.otdengenharia.appotdoac.local.FormularioDispositivoOacViewModel
import com.otdengenharia.appotdoac.local.FotoOacViewModel
import com.otdengenharia.appotdoac.local.ListaDispositivosOacViewModel
import com.otdengenharia.appotdoac.local.db.DatabaseHelper
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ViewModelFactory(private val apiHelper: ApiHelper, private val dbHelper: DatabaseHelper) :
    ViewModelProvider.Factory {

    @RequiresApi(Build.VERSION_CODES.O)
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ListaDispositivosOacViewModel::class.java)) {
            return ListaDispositivosOacViewModel(apiHelper, dbHelper) as T
        }
        else if (modelClass.isAssignableFrom(FormularioDispositivoOacViewModel::class.java)) {
            return FormularioDispositivoOacViewModel(dbHelper) as T
        }
        else if (modelClass.isAssignableFrom(FotoOacViewModel::class.java)) {
            return FotoOacViewModel(dbHelper) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}