package com.otdengenharia.appotdoac.utils

import com.otdengenharia.appotdoac.local.entity.Oac

object FilterUtils {
    fun filter(
        dispositivos : ArrayList<Oac>,
        km: List<Float>,
        segmento: List<Float>,
        checkedChipIdsMontanteJusante: List<String>,
        checkedChipsSentido: List<String>,
        checkedChipsStatus: List<String>
    ): ArrayList<Oac> {
        var result: ArrayList<Oac> = dispositivos
        result = filterKm(result, km)
        result = filterSegmento(result, segmento)
        result = filterMontanteJusante(checkedChipIdsMontanteJusante, result)
        result = filterSentido(checkedChipsSentido, result)
        result = filterStatus(checkedChipsStatus, result)
        return result
    }

    private fun filterStatus(
        checkedChipsStatus: List<String>,
        result: ArrayList<Oac>
    ): ArrayList<Oac> {
        var result1 = result
        if (checkedChipsStatus.isNotEmpty()) {
            result1 = result1.filter {
                it.avaliado.replace(" ","") in checkedChipsStatus
            } as ArrayList<Oac>
        }
        return result1
    }

    private fun filterSentido(
        checkedChipsSentido: List<String>,
        result: ArrayList<Oac>
    ): ArrayList<Oac> {
        var result1 = result
        if (checkedChipsSentido.isNotEmpty()) {
            result1 = result1.filter {
                it.lado.replace(" ","") in checkedChipsSentido
            } as ArrayList<Oac>
        }
        return result1
    }

    private fun filterMontanteJusante(
        checkedChipIdsElemento: List<String>,
        result: ArrayList<Oac>
    ): ArrayList<Oac> {
        var result1 = result
        if (checkedChipIdsElemento.isNotEmpty()) {
            result1 = result1.filter {
                it.montanteJusante.replace(" ","") in checkedChipIdsElemento
            } as ArrayList<Oac>
        }
        return result1
    }

    private fun filterKm(
        result: ArrayList<Oac>,
        km: List<Float>
    ) = result.filter {
        if (it.kmInteiro=="-"){
            it.kmInteiro = 0.toString()
        }
        it.kmInteiro.toInt() >= km.first().toFloat().toInt() && it.kmInteiro.toInt() <= km.last().toFloat().toInt()
    } as ArrayList<Oac>

    private fun filterSegmento(
        result: ArrayList<Oac>,
        segmento: List<Float>
    ) = result.filter {
        it.segmento >= segmento.first().toInt() && it.segmento <= segmento.last().toInt()
    } as ArrayList<Oac>
}